<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Autorizacion de materia</title>
    <style>
        h4{
            color: #3D3D3F; 
            background: #E1E3E8; 
            padding: 1%; 
            text-align:center
        }
        h3{
            color: #444444;
            font-weight: normal;
            font-family: Arial;
            text-transform: uppercase;
            text-align:center;
            margin-bottom: -1%;
        }
    </style>
</head>
<body>
    <div class="row">
        <h3 style="color:#666;"> Detalle de la materia solicitada </h3>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <table class="table  table-striped table-sm" style="width: 100%">
                    <thead>
                        <tr>
                            <td colspan="5"> 
                                <h4>Materia</h4> 
                            </td>
                        </tr>
                        <tr>
                            <th>Clave: </th>
                            <td> {{ $materia[0]->clave_materia }} </td>
                            <td></td>
                            <th>Materia:</th>
                            <td> {{ $materia[0]->materia }} </td>
                        </tr>
                        <tr>
                            <th>Estado:</th>
                            <td>
                                <span class="badge badge-success">Aprobada</span> 
                            </td>
                            <td></td>
                            <th>Creditos:</th>
                            <td> {{ $materia[0]->creditos }} </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <h4>Carrera</h4> 
                            </td>
                        </tr>
                        <tr>
                            <th>Carrera:</th>
                            <td> {{ $materia[0]->carrera }} </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <h4>Coordinador</h4> 
                            </td>
                        </tr>
                        <tr>
                            <th>Coordinador:</th>
                            <td> {{ $materia[0]->name }} </td>
                            <td></td>
                            <th>Email:</th>
                            <td> {{ $materia[0]->email }} </td>
                        </tr>
                        <tr>
                            <th>Direccion:</th>
                            <td> {{ $materia[0]->direccion }} </td>
                            <td></td>
                            <th>Telefono:</th>
                            <td> {{ $materia[0]->telefono }} </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <h4>Profesor</h4> 
                            </td>
                        </tr>
                        <tr>
                            <th>Clave:</th>
                            <td> {{ $materia[0]->clave_profesor }} </td>
                            <td></td>
                            <th>Nombre:</th>
                            <td> {{ $materia[0]->nombre_profesor }} </td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td> {{ $materia[0]->email_profesor }} </td>
                            <td></td>
                            <th>Telefono:</th>
                            <td> {{ $materia[0]->telefono_profesor }} </td>
                        </tr>
                        <tr>
                            <th>Area:</th>
                            <td> {{ $materia[0]->area }} </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <h4></h4> 
                            </td>
                        </tr>
                    </thead> 
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                <p style="float:right; margin-top:2%;">Chilpancingo Guerrero, {{ $dia }} de {{ $mes_nombre }} del {{ $anio }}. </p>
                            </td>
                        </tr>
                    </tfoot>   
                </table>
            </div>
        </div>
    </div>
</body>
</html>