<!DOCTYPE html>
<html>
	<head>
		<title>Inicio</title>
		<meta charset='utf-8'>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="/css/app-all.css" rel="stylesheet">
		<style>
			.fondo{
				width: 100%;
				height: 450px;
				opacity: 0.7;
			}
		</style>
	</head>
	<body style='background: #fff'>
		<div class='container' style='background: rgba(5,4,5,0);' id="app">
			<div class="row" style="background:white">
				<div class="col-12">
					<img src="/images/logo-tec.jpg" width=100%' height='auto'>
				</div>
			</div>
			<div class="row" >
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="container">
                            <div class="card">
                                <div class="card-header"> Recuperar contraseña. </div>
                
                                <div class="card-body">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert"> {{ session('status') }} </div>
                                    @endif
                                    <form method="POST" action="{{ route('password.email') }}">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right"> Email: </label>
                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong> </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                            <div class="col-md-10 offset-md-6">

                                                <button style="float:right" type="submit" class="btn btn-primary">
                                                    Enviar email
                                                </button>
                                                <a class="btn" style="float:right" href="/"> Login </a>
                                            </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                <div class="col-sm-2"></div>
			</div>
		</div>
	</body>

	<script src="/js/app.js"></script>
    <script src="/js/app-all.js"></script>
</html>