<!DOCTYPE html>
<html>
	<head>
		<title>Inicio</title>
		<meta charset='utf-8'>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="/css/app-all.css" rel="stylesheet">
		<style>
			.fondo{
				width: 100%;
				height: 450px;
				opacity: 0.7;
			}
		</style>
	</head>
	<body style='background: #fff'>
		<div class='container' style='background: rgba(5,4,5,0);' id="app">
			<div class="row" style="background:white;">
				<div class="col-12">
					<img src="/images/logo-tec.jpg" width=100%' height='auto'>
				</div>
            </div>
            <div class="container" style="background: #red; padding-top: 3%; padding-bottom: 5%;">
                <div class="row" >
                    <div class="col-md-3"></div>
                    @yield('login')
                    <div class="col-md-3"></div>
                </div>
            </div>
		</div>
	</body>

	<script src="/js/app.js"></script>
    <script src="/js/app-all.js"></script>

</html>