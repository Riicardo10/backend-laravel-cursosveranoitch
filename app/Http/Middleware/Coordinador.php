<?php

namespace App\Http\Middleware;
use Closure;

class Coordinador {
    
    public function handle($request, Closure $next) {
        return $next($request);
    }
}
