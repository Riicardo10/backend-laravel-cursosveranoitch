<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Semestre;

class SemestreController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $semestres = Semestre::orderBy('id', 'asc')->paginate(5);
        else
            $semestres = Semestre::where('semestre', 'like', '%' . $request->buscar . '%')->orderBy('id', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $semestres->total(),
                'pagina_actual' => $semestres->currentPage(),
                'por_pagina' => $semestres->perPage(),
                'ultima_pagina' => $semestres->lastPage(),
                'desde' => $semestres->firstItem(),
                'hasta' => $semestres->lastItem()
            ],
            'semestres' => $semestres
        ];
    }

    public function store(Request $request) {
        $semestre = new Semestre();
        $semestre->semestre = $request->semestre;
        $semestre->save();
    }

    public function update(Request $request, $id) {
        $semestre = Semestre::findOrFail( $request->id );
        $semestre->semestre = $request->semestre;
        $semestre->save();   
    }

    public function getSemestres() {
        $semestres = Semestre::all();
        return [ 'semestres' => $semestres ];
    }
}
