<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Area;

class AreaController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $areas = Area::orderBy('updated_at', 'desc')->paginate(5);
        else
            $areas = Area::where('area', 'like', '%' . $request->buscar . '%')->orderBy('updated_at', 'desc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $areas->total(),
                'pagina_actual' => $areas->currentPage(),
                'por_pagina' => $areas->perPage(),
                'ultima_pagina' => $areas->lastPage(),
                'desde' => $areas->firstItem(),
                'hasta' => $areas->lastItem()
            ],
            'areas' => $areas
        ];
    }

    public function store(Request $request) {
        $area = new Area();
        $area->area = $request->area;
        $area->save();
        return [ 'area' => $area ];
    }

    public function update(Request $request, $id) {
        $area = Area::findOrFail( $id );
        $area->area = $request->area;
        $area->save();   
        return [ 'area' => $area ];
    }

    public function getAreas() {
        $areas = Area::all();
        return [ 'areas' => $areas ];
    }
}
