<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\MateriaSolicitada;

class MateriaSolicitadaController extends Controller {
    
    public function store(Request $request) {
        $anio_actual = date('Y'); 
        $materias = $request->datos['claves'];
        for($i=0; $i<count($materias); $i++) {
            $materia_solicitada = new MateriaSolicitada();
            $materia_solicitada->id_coordinador = $request->datos['usuario'];
            $materia_solicitada->clave_materia = $materias[$i];
            $materia_solicitada->aprobada = '2';
            $materia_solicitada->anio = $anio_actual;
            $materia_solicitada->save();
        }
    }
    public function getCantidadMateriasSolicitadas(Request $request, $id) {
        $anio_actual = date('Y'); 
        $cantidad = MateriaSolicitada::where('id_coordinador', '=', $id)->where('anio', '=', $anio_actual)->count();
        return [ 'cantidad' => $cantidad ];
    }
    public function getMateriasSolicitadas(Request $request, $id) {
        $anio_actual = date('Y'); 
        $materias = MateriaSolicitada::join('materias', 'materias_solicitadas.clave_materia', '=', 'materias.clave')
        ->join('carreras', 'materias.id_carrera','=','carreras.id')
        ->select('materias_solicitadas.id as id_materia_solicitada', 'materias_solicitadas.clave_materia', 'materias.materia', 'materias_solicitadas.aprobada', 'carreras.carrera')
        ->where('id_coordinador', '=', $id)->where('anio', '=', $anio_actual)
        ->orderBy('materias.clave', 'asc')->get();
        return [ 'materias' => $materias ];
    }
    public function getMateriasSolicitadasJefeDepartamento(Request $request) {
        $anio_actual = date('Y'); 
        $materias = MateriaSolicitada::join('materias', 'materias_solicitadas.clave_materia', '=', 'materias.clave')
        ->join('carreras', 'materias.id_carrera','=','carreras.id')
        ->join('users', 'users.id','=','materias_solicitadas.id_coordinador')
        ->leftJoin('profesores', 'profesores.clave','=','materias_solicitadas.clave_profesor')
        ->leftJoin('areas', 'profesores.id_area','=','areas.id')
        ->select('materias_solicitadas.id', 'materias_solicitadas.id_coordinador',
                 'materias_solicitadas.clave_materia', 'materias.materia','materias.creditos', 
                 'materias_solicitadas.aprobada', 'carreras.carrera', 
                 'materias_solicitadas.clave_profesor', 'users.name', 'users.email',
                 'users.direccion', 'users.telefono', 'profesores.clave as clave_profesor', 'profesores.nombre as nombre_profesor',
                 'profesores.email as email_profesor', 'profesores.telefono as telefono_profesor', 'areas.area')
        ->where('materias_solicitadas.anio', '=', $anio_actual)
        ->orderBy('materias_solicitadas.id', 'asc')->paginate(4);
        return [
            'paginacion' => [
                'total' => $materias->total(),
                'pagina_actual' => $materias->currentPage(),
                'por_pagina' => $materias->perPage(),
                'ultima_pagina' => $materias->lastPage(),
                'desde' => $materias->firstItem(),
                'hasta' => $materias->lastItem()
            ],
            'materias' => $materias
        ];
    }
    public function aprobarMateria(Request $request, $clave, $clave_profesor, $estado) {
        $materia_solicitada = MateriaSolicitada::findOrFail( $clave );
        $materia_solicitada->aprobada = $estado;
        if( $estado == 1 ) {
            $materia_solicitada->clave_profesor = $clave_profesor;    
        }
        $materia_solicitada->save();
    }
    public function rechazarMateria(Request $request) {
        $materia_solicitada = MateriaSolicitada::findOrFail( $request->id );
        $materia_solicitada->aprobada = 0;
        $materia_solicitada->save();
    }

    public function getMateriasSolicitadasPDF( Request $request, $id ){
        $anio = date('Y'); 
        $mes = date('m') * 1; 
        $dia = date('d'); 
        $hora = date('H:i:s'); 
        $materia = MateriaSolicitada::join('materias', 'materias_solicitadas.clave_materia', '=', 'materias.clave')
        ->join('carreras', 'materias.id_carrera','=','carreras.id')
        ->join('users', 'users.id','=','materias_solicitadas.id_coordinador')
        ->leftJoin('profesores', 'profesores.clave','=','materias_solicitadas.clave_profesor')
        ->leftJoin('areas', 'profesores.id_area','=','areas.id')
        ->select('materias_solicitadas.id', 'materias_solicitadas.id_coordinador',
                 'materias_solicitadas.clave_materia', 'materias.materia','materias.creditos', 
                 'materias_solicitadas.aprobada', 'carreras.carrera', 
                 'materias_solicitadas.clave_profesor', 'users.name', 'users.email',
                 'users.direccion', 'users.telefono', 'profesores.clave as clave_profesor', 'profesores.nombre as nombre_profesor',
                 'profesores.email as email_profesor', 'profesores.telefono as telefono_profesor', 'areas.area')
        ->where('materias_solicitadas.anio', '=', $anio)
        ->where('materias_solicitadas.id', '=', $id)
        ->take(1)->get();
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre', 'Noviembre','Diciembre'];
        $pdf = \PDF::loadView( 'pdf.materia_solicitada', [
            'materia'=>$materia, 
            'anio'=>$anio,
            'mes_numero'=>$mes,
            'mes_nombre'=>$meses[$mes-1],
            'dia'=>$dia,
            'hora'=>$hora
        ] );
        return $pdf->download( 'materia_solicitada.pdf' );
    }

}