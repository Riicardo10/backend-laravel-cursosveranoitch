<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Profesor;

class ProfesorController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $profesores = Profesor::join('areas', 'profesores.id_area', '=', 'areas.id')
            ->select(   'profesores.clave', 'profesores.nombre as profesor', 'profesores.email',
                        'profesores.telefono', 'profesores.estado', 'areas.id as id_area', 'areas.area')
            ->orderBy('clave', 'asc')->paginate(5);
        else
            $profesores = Profesor::join('areas', 'profesores.id_area', '=', 'areas.id')
            ->select(   'profesores.clave', 'profesores.nombre as profesor', 'profesores.email',
                        'profesores.telefono', 'profesores.estado', 'areas.id as id_area', 'areas.area')
            ->where('nombre', 'like', '%' . $request->buscar . '%')
            ->orderBy('clave', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $profesores->total(),
                'pagina_actual' => $profesores->currentPage(),
                'por_pagina' => $profesores->perPage(),
                'ultima_pagina' => $profesores->lastPage(),
                'desde' => $profesores->firstItem(),
                'hasta' => $profesores->lastItem()
            ],
            'profesores' => $profesores
        ];
    }

    public function store(Request $request) {
        $profesor = new Profesor();
        $profesor->clave = $request->profesor['clave'];
        $profesor->nombre = $request->profesor['nombre'];
        $profesor->email = $request->profesor['email'];
        $profesor->telefono = $request->profesor['telefono'];
        $profesor->id_area = $request->profesor['id_area'];
        $profesor->save();
    }

    public function update(Request $request, $clave) {
        Profesor::where( 'clave', $clave )->update( [
            'nombre'=>$request->profesor['nombre'],
            'email'=>$request->profesor['email'],
            'telefono'=>$request->profesor['telefono'],
            'id_area'=>$request->profesor['id_area']
        ] );
    }

    public function getProfesores() {
        $profesores = Profesor::join('areas', 'profesores.id_area', '=', 'areas.id')
            ->select(   'profesores.clave', 'profesores.nombre as profesor', 'profesores.email',
                        'profesores.telefono', 'profesores.estado', 'areas.id as id_area', 'areas.area')
            ->where('profesores.estado', '=', '1')
            ->orderBy('profesores.updated_at', 'desc')->get();
        return [ 'profesores' => $profesores ];
    }

    public function desactivar(Request $request, $clave) {
        Profesor::where( 'clave', $clave )->update( [
            'estado'=>0
        ] );
    }

    public function activar(Request $request, $clave) {
        Profesor::where( 'clave', $clave )->update( [
            'estado'=>1
        ] );
    }

    
}
