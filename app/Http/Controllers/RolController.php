<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rol;

class RolController extends Controller {
    public function index( Request $request ) {
        // if(!$request->ajax()) return redirect( '/principal' );
        if( $request->buscar == '' ) 
            $roles = Rol::orderBy('rol', 'asc')->paginate(3);
        else
            $roles = Rol::where('rol', 'like', '%' . $request->buscar . '%')->orderBy('rol', 'asc')->paginate(3);
        return [
            'paginacion' => [
                'total' => $roles->total(),
                'pagina_actual' => $roles->currentPage(),
                'por_pagina' => $roles->perPage(),
                'ultima_pagina' => $roles->lastPage(),
                'desde' => $roles->firstItem(),
                'hasta' => $roles->lastItem()
            ],
            'roles' => $roles
        ];
    }

    public function store(Request $request) {
        $rol = new Rol();
        $rol->rol = $request->rol['rol'];
        $rol->descripcion = $request->rol['descripcion'];
        $rol->save();
    }

    public function update(Request $request, $id) {
        $rol = Rol::findOrFail( $id );
        $rol->rol = $request->rol;
        $rol->descripcion = $request->descripcion;
        $rol->save();   
    }

    public function getRoles() {
        $roles = Rol::where('estado', '=', 1)->get();
        return [ 'roles' => $roles ];
    }

    public function desactivar(Request $request) {
        $rol = Rol::findOrFail( $request->id );
        $rol->estado = '0';
        $rol->save();
    }

    public function activar(Request $request) {
        $rol = Rol::findOrFail( $request->id );
        $rol->estado = '1';
        $rol->save();
    }
}
