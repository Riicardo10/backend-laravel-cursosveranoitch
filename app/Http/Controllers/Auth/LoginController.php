<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use JWTAuth;

class LoginController extends Controller {

    public function showLoginForm(Request $request) {
    }
    
    public function login(Request $request) {
        $this->validateLogin( $request );
        if ( ! $token = JWTAuth::attempt( ['email' => $request->email, 'password' => $request->password] )) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }
        $usuario = UserController::getUsuarioByEmail( $request->email );
        return response( ['token'=>$token, 'usuario' => $usuario,] );
        /*return response.json([
                'status' => 'success',
                '_token' => $token
            ])
            ->header('Authorization', $token);

        return;



        $attempt = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
        
        if( Auth::attempt( ['email'=>$request->email, 'password'=>$request->password, 'estado'=>1] ) ) {
            return 'GENIAL';
        }
        
        return back()
                ->withErrors( ['email'=>trans( 'auth.failed' )] )
                ->withInput( request( ['email'] ) );*/
    }

    protected function validateLogin(Request $request) {
        $this->validate( $request, [
            'email' => 'required|string',
            'password' => 'required|string'
        ] );
    }

    public function logout(Request $request) {
        Auth::logout();
        $request->session()->invalidate();
        return redirect( '/' );
    }
}
