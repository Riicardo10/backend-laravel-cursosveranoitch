<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Materia;

class MateriaController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $materias = Materia::join('carreras', 'materias.id_carrera', '=','carreras.id')
            ->join('semestres', 'materias.id_semestre','=','semestres.id')
            ->select('materias.clave','materias.materia','materias.creditos','carreras.id as id_carrera','carreras.carrera as carrera','semestres.id as id_semestre','semestres.semestre')
            ->orderBy('materias.id_semestre', 'asc')
            ->orderBy('materias.materia', 'asc')
            ->orderBy('carreras.carrera', 'asc')->paginate(5);
        else
            $materias = Materia::join('carreras', 'materias.id_carrera', '=','carreras.id')
            ->join('semestres', 'materias.id_semestre','=','semestres.id')
            ->select('materias.clave','materias.materia','materias.creditos','carreras.id as id_carrera','carreras.carrera as carrera','semestres.id as id_semestre','semestres.semestre')
            ->where('materia', 'like', '%' . $request->buscar . '%')
            ->orderBy('materias.id_semestre', 'asc')
            ->orderBy('materias.materia', 'asc')
            ->orderBy('carreras.carrera', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $materias->total(),
                'pagina_actual' => $materias->currentPage(),
                'por_pagina' => $materias->perPage(),
                'ultima_pagina' => $materias->lastPage(),
                'desde' => $materias->firstItem(),
                'hasta' => $materias->lastItem()
            ],
            'materias' => $materias
        ];
    }

    public function store(Request $request) {
        $materia = new Materia();
        $materia->clave = $request->materia['clave'];
        $materia->materia = $request->materia['materia'];
        $materia->creditos = $request->materia['creditos'];
        $materia->id_carrera = $request->materia['id_carrera'];
        $materia->id_semestre = $request->materia['id_semestre'];
        $materia->save();
    }

    public function update(Request $request, $clave) {
        Materia::where( 'clave', $clave )->update( [
            'materia'=>$request->materia,
            'creditos'=>$request->creditos,
            'id_carrera'=>$request->id_carrera,
            'id_semestre'=>$request->id_semestre
        ] );
    }

    public function getMaterias() {
        $materias = Materia::all();
        return [ 'materias' => $materias ];
    }
}
