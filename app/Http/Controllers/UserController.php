<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {

    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
            ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                        'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
            ->orderBy('name', 'asc')->paginate(5);
        else
            $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
            ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                        'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
            ->where('users.' . $request->criterio, 'like', '%' . $request->buscar . '%')
            ->orderBy('name', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $usuarios->total(),
                'pagina_actual' => $usuarios->currentPage(),
                'por_pagina' => $usuarios->perPage(),
                'ultima_pagina' => $usuarios->lastPage(),
                'desde' => $usuarios->firstItem(),
                'hasta' => $usuarios->lastItem()
            ],
            'usuarios' => $usuarios,
            
        ];
    }

    public function store(Request $request) {
        $usuario = new User();
        $usuario->name = $request->usuario['usuario'];
        $usuario->email = $request->usuario['email'];
        $usuario->password = bcrypt($request->usuario['password']);
        $usuario->direccion = $request->usuario['direccion'];
        $usuario->telefono = $request->usuario['telefono'];
        $usuario->id_rol = $request->usuario['id_rol'];
        $usuario->save();
    }

    public function update(Request $request, $id) {
        $usuario = User::findOrFail( $id );
        $usuario->direccion = $request->usuario['direccion'];
        $usuario->telefono = $request->usuario['telefono'];
        $usuario->id_rol = $request->usuario['id_rol'];
        $usuario->save();   
    }

    public function desactivar(Request $request, $id) {
        $user = User::findOrFail( $id );
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request, $id) {
        $user = User::findOrFail( $id );
        $user->estado = '1';
        $user->save();
    }
    public function getUsuarioByRol(Request $request) {
        
        $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
        ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                    'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
        ->where('users.id_rol', '=', $request->id_rol)
        ->orderBy('users.name', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $usuarios->total(),
                'pagina_actual' => $usuarios->currentPage(),
                'por_pagina' => $usuarios->perPage(),
                'ultima_pagina' => $usuarios->lastPage(),
                'desde' => $usuarios->firstItem(),
                'hasta' => $usuarios->lastItem()
            ],
            'usuarios' => $usuarios
        ];
    }

    public static function getUsuarioByEmail( $email ) {
        $usuario = User::join('roles', 'users.id_rol', '=','roles.id')
        ->select(   'users.id','users.name','users.email','users.telefono','users.direccion',
                    'users.email','users.estado', 'roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
        ->where('users.email', '=', $email)
        ->get();
        return [
            'usuario' => $usuario
        ];
    }

}
