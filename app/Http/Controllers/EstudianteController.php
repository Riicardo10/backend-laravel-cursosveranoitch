<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Estudiante;

class EstudianteController extends Controller {
    
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $estudiantes = Estudiante::join('carreras', 'estudiantes.id_carrera', '=', 'carreras.id')
            ->select(   'estudiantes.matricula', 'estudiantes.nombre as estudiante', 'estudiantes.email',
                        'estudiantes.telefono', 'carreras.id as id_carrera', 'carreras.carrera')
            ->orderBy('matricula', 'asc')->paginate(5);
        else
            $estudiantes = Estudiante::join('carreras', 'estudiantes.id_carrera', '=', 'carreras.id')
            ->select(   'estudiantes.matricula', 'estudiantes.nombre as estudiante', 'estudiantes.email',
                        'estudiantes.telefono', 'carreras.id as id_carrera', 'carreras.carrera')
            ->where('estudiantes.nombre', 'like', '%' . $request->buscar . '%')
            ->orderBy('matricula', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $estudiantes->total(),
                'pagina_actual' => $estudiantes->currentPage(),
                'por_pagina' => $estudiantes->perPage(),
                'ultima_pagina' => $estudiantes->lastPage(),
                'desde' => $estudiantes->firstItem(),
                'hasta' => $estudiantes->lastItem()
            ],
            'estudiantes' => $estudiantes
        ];
    }

    public function store(Request $request) {
        $estudiante = new Estudiante();
        $estudiante->matricula = $request->estudiante['matricula'];
        $estudiante->nombre = $request->estudiante['nombre'];
        $estudiante->email = $request->estudiante['email'];
        $estudiante->telefono = $request->estudiante['telefono'];
        $estudiante->id_carrera = $request->estudiante['id_carrera'];
        $estudiante->save();
    }

    public function update(Request $request, $matricula) {
        Estudiante::where( 'matricula', $matricula )->update( [
            'nombre'=>$request->estudiante['nombre'],
            'email'=>$request->estudiante['email'],
            'telefono'=>$request->estudiante['telefono'],
            'id_carrera'=>$request->estudiante['id_carrera']
        ] );
    }

    public function getEstudiante(Request $request, $matricula) {
        $estudiante = Estudiante::join('carreras', 'estudiantes.id_carrera', '=', 'carreras.id')
            ->select(   'estudiantes.matricula', 'estudiantes.nombre as estudiante', 'estudiantes.email',
                        'estudiantes.telefono', 'carreras.id as id_carrera', 'carreras.carrera')
            ->where('estudiantes.matricula', '=', $matricula)->get();
        return [
            'estudiante' => $estudiante
        ];
    }

}
