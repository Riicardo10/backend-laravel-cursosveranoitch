<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;


class CustomResetPasswordNotification extends Notification {
    
    public $token;

    public static $toMailCallback;

    public function __construct($token) {
        $this->token = $token;
    }

    public function via($notifiable) {
        return ['mail'];
    }

    public function toMail($notifiable) {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        return (new MailMessage)
            ->subject(Lang::getFromJson('Notificación de cambio de contraseña'))
            ->line(Lang::getFromJson('Estás recibiendo éste email porque recibimos una solicitud de cambio de contraseña para tu cuenta.'))
            ->action(Lang::getFromJson('Cambiar contraseña'), url(config('app.url').route('password.reset', $this->token, false)))
            ->line(Lang::getFromJson('Si no solicitaste el cambio de contraseña, no hagas ninguna acción adicional.'));
    }

    public static function toMailUsing($callback){
        static::$toMailCallback = $callback;
    }
}